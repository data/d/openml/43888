# OpenML dataset: communities-and-crime-binary

https://www.openml.org/d/43888

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Communities and Crime (Binarized) 
 The following changes were introduced to OpenML Dataset 315.
 * binarized 'racepctblack' at 0.06 
 binarized 'ViolentCrimesPerPop' at 0.2

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43888) of an [OpenML dataset](https://www.openml.org/d/43888). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43888/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43888/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43888/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

